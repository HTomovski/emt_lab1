package com.emt.km.lab1.assignment.web;

import com.emt.km.lab1.assignment.model.Product;
import com.emt.km.lab1.assignment.model.exceptions.MissingProductParameterException;
import com.emt.km.lab1.assignment.model.exceptions.NoSuchCategoryException;
import com.emt.km.lab1.assignment.model.exceptions.NoSuchManufacturerException;
import com.emt.km.lab1.assignment.model.exceptions.NoSuchProductException;
import com.emt.km.lab1.assignment.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;


@Controller
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ModelAndView getProducts() {
        ModelAndView modelAndView = new ModelAndView("products");
        modelAndView.addObject("productsList", productService.getProducts());
        return modelAndView;
    }

    @ExceptionHandler({NoSuchProductException.class})
    @GetMapping("/{productId}")
    public ModelAndView getProductDetailed(@PathVariable("productId") Long productId) throws NoSuchProductException {
        ModelAndView modelAndView = new ModelAndView("productDetailed");
        Optional<Product> product = productService.getProduct(productId);
        product.ifPresent(value -> modelAndView.addObject("product", value));
        return modelAndView;
    }

    @GetMapping("/add")
    public ModelAndView addProduct() {
        ModelAndView modelAndView = new ModelAndView("addProduct");
        modelAndView.addObject("product", new Product());
        modelAndView.addObject("categoriesList", productService.getCategories());
        modelAndView.addObject("manufacturersList", productService.getManufacturers());
        modelAndView.addObject("accessories", productService.getAccessories());
        return modelAndView;
    }

    @PostMapping("/add")
    public String addProduct(@ModelAttribute Product product/*,
                             @RequestParam("name") String name,
                             @RequestParam("price") int price,
                             @RequestParam("description") String description,
                             @RequestParam("imgSrc") String imgSrc,
                             @RequestParam("category") Category category,
                             @RequestParam("manufacturer") Manufacturer manufacturer,
                             @RequestParam(value = "accessories", required = false) List<Accessory> accessories*/) throws NoSuchManufacturerException, MissingProductParameterException, NoSuchCategoryException {
        //productService.addProduct(name, description, imgSrc, category, manufacturer, accessories);
        productService.addProduct(product);
        return "redirect:/products/";
    }

    @DeleteMapping("/")
    public String deleteProduct(@RequestParam("productId") Long productId) {
        productService.deleteProduct(productId);
        return "redirect:/products/";
    }
/*
    @RequestMapping(value = "image/{imageName}")
    @ResponseBody
    public byte[] getImage(@PathVariable(value = "imageName") String imageName) throws IOException {
        File serverFile = new File("main\\resources\\static\\images\\" + imageName);


        return Files.readAllBytes(Paths.get(resource.getURI()));
    }
*/
/*
    @RequestMapping(value = "productImage/{productId}")
    @ResponseBody
    public ResponseEntity<byte[]> getProductImage(@PathVariable("productId") Long productId) throws IOException {
        Product product = productService.findById(productId);


        byte[] imageContent = product.getPhoto();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDisposition(ContentDisposition.parse("attachment; filename="+ product.getName()));
        headers.setContentLength(imageContent.length);
        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(imageContent.length)
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(imageContent);
    }
*/

}
