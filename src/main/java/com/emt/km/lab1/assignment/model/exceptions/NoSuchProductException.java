package com.emt.km.lab1.assignment.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus( code = HttpStatus.NOT_FOUND, reason = "Product does not exist.")
public class NoSuchProductException extends RuntimeException {
}
