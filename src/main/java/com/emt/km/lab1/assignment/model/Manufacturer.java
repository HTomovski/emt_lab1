package com.emt.km.lab1.assignment.model;

import javax.persistence.*;

@Entity
@Table(name = "manufacturer")
public class Manufacturer {
    //private static Long idCounter = 0L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;

    public Manufacturer() {
    }
/*
    public Manufacturer(String name) {
        this.id = idCounter++;
        this.name = name;
    }
*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
