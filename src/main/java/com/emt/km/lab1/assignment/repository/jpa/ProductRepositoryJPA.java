package com.emt.km.lab1.assignment.repository.jpa;

import com.emt.km.lab1.assignment.model.Accessory;
import com.emt.km.lab1.assignment.model.Category;
import com.emt.km.lab1.assignment.model.Manufacturer;
import com.emt.km.lab1.assignment.model.Product;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@Profile({"mysql-db", "h2-db"})
public interface ProductRepositoryJPA extends JpaRepository<Product, Long> {

    @Query(value = "select c from com.emt.km.lab1.assignment.model.Category c")
    List<Category> findAllCategories();

    @Query(value = "select m from com.emt.km.lab1.assignment.model.Manufacturer m")
    List<Manufacturer> findAllManufacturers();

    @Query(value = "select a from com.emt.km.lab1.assignment.model.Accessory a")
    List<Accessory> findAllAccessories();

    @Query(value = "select c from com.emt.km.lab1.assignment.model.Category c where c=:catId")
    Optional<Category> findCategoryById(@Param(value = "catId")Long categoryId);

    @Query(value = "select m from com.emt.km.lab1.assignment.model.Manufacturer m where m=:manId")
    Optional<Manufacturer> findManufacturerById(@Param(value = "manId")Long manufacturerId);

    Collection<Product> findAllByCategory(Category category);

    Collection<Product> findAllByManufacturer(Manufacturer manufacturer);

    Collection<Product> findAllByCategoryAndManufacturer(Category category, Manufacturer manufacturer);


    @Query(value = "select p from Product p where p.category.id =:catId")
    Collection<Product> findAllByCategory(@Param("catId") Long categoryId);

    @Query(value = "select p from Product p where p.manufacturer.id =:catId")
    Collection<Product> findAllByManufacturer(@Param("catId") Long categoryId);

    @Query(value = "select p from Product p where p.category.id=:catId and p.manufacturer.id=:manId")
    Collection<Product> findAllByCategoryAndManufacturer(@Param("catId") Long categoryId,
                                                         @Param("manId") Long manufacturerId);


    //todo: Migrate this method to its own repository class.
    @Query(value = "select a from Accessory a where a.id=:id")
    Optional<Accessory> findAccessoryById(@Param("id") Long id);

    @Query(value = "select sum(p.price) from com.emt.km.lab1.assignment.model.Product p where p.category.id=:catId")
    Integer getTotalPriceForCategory(@Param(value = "catId") Long categoryId);


}
