package com.emt.km.lab1.assignment.web;

import com.emt.km.lab1.assignment.model.Product;
import com.emt.km.lab1.assignment.model.dto.ChargeRequest;
import com.emt.km.lab1.assignment.model.exceptions.NoSuchProductException;
import com.emt.km.lab1.assignment.service.PaymentService;
import com.emt.km.lab1.assignment.service.ProductService;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;

/**
 * Created by HTomovski
 * Date: 15-May-19
 * Time: 1:41 PM
 */
@Controller
public class PaymentController {

    @Value("${STRIPE_PUBLIC_KEY}")
    private String stripePublicKey;

    private final PaymentService paymentService;

    private final ProductService productService;

    public PaymentController(PaymentService paymentService, ProductService productService) {
        this.paymentService = paymentService;
        this.productService = productService;
    }

    @GetMapping("/checkout/{id}")
    public String checkout(@PathVariable("id") Long id,
                           Model model) throws NoSuchProductException {
        Product product;
        Optional<Product> productOptional = productService.getProduct(id);
        if (productOptional.isPresent())
            product = productOptional.get();
        else throw new NoSuchProductException();

        model.addAttribute("name", product.getName());
        model.addAttribute("amount", product.getPrice() * 100); // in cents
        model.addAttribute("stripePublicKey", stripePublicKey);
        model.addAttribute("currency", ChargeRequest.Currency.EUR);
        return "checkout";
    }

    @PostMapping("/charge")
    public String charge( ChargeRequest chargeRequest, Model model)
            throws StripeException {

        chargeRequest.setDescription("EMT payment");
        chargeRequest.setCurrency(ChargeRequest.Currency.EUR);
        Charge charge = paymentService.charge(chargeRequest);
        model.addAttribute("id", charge.getId());
        model.addAttribute("status", charge.getStatus());
        model.addAttribute("chargeId", charge.getId());
        model.addAttribute("balance_transaction", charge.getBalanceTransaction());
        return "result";
    }


    @ExceptionHandler(StripeException.class)
    public String handleError(Model model, StripeException ex) {
        model.addAttribute("error", ex.getMessage());
        return "result";
    }
}
