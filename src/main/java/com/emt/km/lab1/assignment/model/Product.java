package com.emt.km.lab1.assignment.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "product")
public class Product {

    private static Long idCounter = 0L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private int price;
    private String description;
    private String imgSrc;
    @ManyToOne
    @JoinColumn(name = "cat_id")
    private Category category;
    @ManyToOne
    @JoinColumn(name = "man_id")
    private Manufacturer manufacturer;
    @ManyToMany
    @JoinTable(name = "product_accessory",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "accessory"))
    private List<Accessory> accessories;


    public Product() {
    }

    public Product(String name, String description, String imgSrc, Category category, Manufacturer manufacturer, int price, List<Accessory> accessories) {
        //this.id = idCounter++;
        this.name = name;
        this.description = description;
        this.imgSrc = imgSrc;
        this.category = category;
        this.manufacturer = manufacturer;
        this.price = price;
        this.accessories = new ArrayList<>();
        if (accessories == null)
            return;
        for (int i = 0; i < accessories.size(); i++) {
            this.accessories.set(i, accessories.get(i));
        }
        //this.accessories = accessories;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public List<Accessory> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<Accessory> accessories) {
        this.accessories = accessories;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
