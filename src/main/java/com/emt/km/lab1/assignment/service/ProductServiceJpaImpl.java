package com.emt.km.lab1.assignment.service;

import com.emt.km.lab1.assignment.model.Accessory;
import com.emt.km.lab1.assignment.model.Category;
import com.emt.km.lab1.assignment.model.Manufacturer;
import com.emt.km.lab1.assignment.model.Product;
import com.emt.km.lab1.assignment.model.exceptions.MissingProductParameterException;
import com.emt.km.lab1.assignment.model.exceptions.NoSuchCategoryException;
import com.emt.km.lab1.assignment.model.exceptions.NoSuchManufacturerException;
import com.emt.km.lab1.assignment.model.exceptions.NoSuchProductException;
import com.emt.km.lab1.assignment.repository.jpa.ProductRepositoryJPA;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Profile({"mysql-db", "h2-db"})
public class ProductServiceJpaImpl implements ProductService {
    private final ProductRepositoryJPA repository;

    public ProductServiceJpaImpl(ProductRepositoryJPA repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Product> getProduct(Long productId) throws NoSuchProductException {
        return repository.findById(productId);
    }

    @Override
    public List<Product> getProducts() {
        return repository.findAll();
    }

    @Override
    public void addProduct(String productName, String productDescription, String imgSrc, Category category, Manufacturer manufacturer, int price, List<Accessory> accessories) throws MissingProductParameterException, NoSuchCategoryException, NoSuchManufacturerException {
        repository.save(new Product(productName, productDescription, imgSrc, category, manufacturer, price, accessories));
    }

    @Override
    public void addProduct(Product product) {
        repository.save(product);
    }

    @Override
    public List<Category> getCategories() {
        return repository.findAllCategories();
    }

    @Override
    public List<Manufacturer> getManufacturers() {
        return repository.findAllManufacturers();
    }

    @Override
    public List<Accessory> getAccessories() {
        return repository.findAllAccessories();
    }

    @Override
    public Optional<Category> getCategoryById(Long categoryId) {
        return repository.findCategoryById(categoryId);
    }

    @Override
    public Optional<Manufacturer> getManufacturerById(Long manufacturerId) {
        return repository.findManufacturerById(manufacturerId);
    }

    @Override
    public void deleteProduct(Long productId) throws NoSuchProductException {
        repository.deleteById(productId);
    }

    @Override
    public List<Product> getProductsByCategory(Long categoryId) throws NoSuchCategoryException {
        return (List<Product>) repository.findAllByCategory(categoryId);
        /*
        Optional<Category> cat = repository.findCategoryById(categoryId);
        if (cat.isPresent())
            return (List<Product>)repository.findAllByCategory(cat.get());
        else throw new NoSuchCategoryException();
        */
    }

    @Override
    public Collection<Product> getProductsByManufacturer(Long manufacturerId) throws NoSuchManufacturerException {
        return repository.findAllByManufacturer(manufacturerId);
        /*
        Optional<Manufacturer> man = repository.findManufacturerById(manufacturerId);
        if (man.isPresent())
            return repository.findAllByManufacturer(man.get());
        else throw new NoSuchManufacturerException();
        */
    }

    @Override
    public Collection<Product> getProductsByCategoryAndManufacturer(Long categoryId, Long manufacturerId) throws NoSuchCategoryException, NoSuchManufacturerException {
        return repository.findAllByCategoryAndManufacturer(categoryId, manufacturerId);
        /*
        Optional<Manufacturer> man = repository.findManufacturerById(manufacturerId);
        Optional<Category> cat = repository.findCategoryById(categoryId);
        if (man.isPresent()) {
            if (cat.isPresent()) {
                return repository.findAllByCategoryAndManufacturer(cat.get(), man.get());
            }
            else {
                throw new NoSuchCategoryException();
            }
        }
        else {
            throw new NoSuchManufacturerException();
        }
        */
    }

    @Override
    public Integer getTotalPriceForCategory(Long categoryId) {
        return repository.getTotalPriceForCategory(categoryId);
    }

    @Override
    public Optional<Accessory> getAccessoryById(Long id) {
        return repository.findAccessoryById(id);
    }
}
