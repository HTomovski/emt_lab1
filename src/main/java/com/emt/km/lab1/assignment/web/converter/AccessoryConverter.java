package com.emt.km.lab1.assignment.web.converter;

import com.emt.km.lab1.assignment.model.Accessory;
import com.emt.km.lab1.assignment.model.exceptions.NoSuchAccessoryException;
import com.emt.km.lab1.assignment.service.ProductService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AccessoryConverter implements Converter<String, Accessory> {
    private final ProductService productService;

    public AccessoryConverter(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public Accessory convert(String stringId) {
        Optional<Accessory> accessory = productService.getAccessoryById(Long.parseLong(stringId));
        if (accessory.isPresent())
            return accessory.get();
        else throw new NoSuchAccessoryException();
    }
}
