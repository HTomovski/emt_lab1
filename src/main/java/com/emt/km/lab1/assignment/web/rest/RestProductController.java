package com.emt.km.lab1.assignment.web.rest;

import com.emt.km.lab1.assignment.model.Product;
import com.emt.km.lab1.assignment.model.exceptions.NoSuchProductException;
import com.emt.km.lab1.assignment.service.ProductService;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/rest")
public class RestProductController {
    private final ProductService productService;

    public RestProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    public Collection<Product> getProducts() {
        return productService.getProducts();
    }

    @ExceptionHandler({NoSuchProductException.class})
    @GetMapping("/products/{productId}")
    public Product getProductDetailed(@PathVariable("productId") Long productId) throws NoSuchProductException {
        Optional<Product> product = productService.getProduct(productId);
        if (product.isPresent())
            return product.get();
        else throw new NoSuchProductException();
    }

    @GetMapping("/product/category/{categoryId}")
    public Collection<Product> getByCategory(@PathVariable("categoryId") int catId) {
        return productService.getProductsByCategory(Integer.toUnsignedLong(catId));
    }

    @GetMapping("/product/manufacturer/{manufacturerId}")
    public Collection<Product> getByManufacturer(@PathVariable("manufacturerId") int manId) {
        return productService.getProductsByManufacturer(Integer.toUnsignedLong(manId));
    }

    @GetMapping("/product/category/{categoryId}/manufacturer/{manufacturerId}")
    public Collection<Product> getByCategoryAndManufacturer(@PathVariable("categoryId") int catId,
                                                            @PathVariable("manufacturerId") int manId) {
        return productService.getProductsByCategoryAndManufacturer(Integer.toUnsignedLong(catId), Integer.toUnsignedLong(manId));
    }

    @GetMapping("/product/category/{categoryId}/price")
    public int getTotalPriceForCategory(@PathVariable("categoryId") int catId) {
        Integer total = productService.getTotalPriceForCategory(Integer.toUnsignedLong(catId));
        if (total == null)
            return 0;
        return total;
    }
}
