package com.emt.km.lab1.assignment.service;

import com.emt.km.lab1.assignment.model.Accessory;
import com.emt.km.lab1.assignment.model.Category;
import com.emt.km.lab1.assignment.model.Manufacturer;
import com.emt.km.lab1.assignment.model.Product;
import com.emt.km.lab1.assignment.model.exceptions.MissingProductParameterException;
import com.emt.km.lab1.assignment.model.exceptions.NoSuchCategoryException;
import com.emt.km.lab1.assignment.model.exceptions.NoSuchManufacturerException;
import com.emt.km.lab1.assignment.model.exceptions.NoSuchProductException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public interface ProductService {

    Optional<Product> getProduct(Long productId) throws NoSuchProductException;

    List<Product> getProducts();

    void addProduct(String productName,
                    String productDescription,
                    String imgSrc,
                    Category category,
                    Manufacturer manufacturer,
                    int price,
                    List<Accessory> accessories)
            throws MissingProductParameterException,
            NoSuchCategoryException,
            NoSuchManufacturerException;

    void addProduct(Product product);

    List<Category> getCategories();

    List<Manufacturer> getManufacturers();

    List<Accessory> getAccessories();

    Optional<Category> getCategoryById(Long categoryId);

    Optional<Manufacturer> getManufacturerById(Long manufacturerId);

    void deleteProduct(Long productId)
            throws NoSuchProductException;

    List<Product> getProductsByCategory(Long categoryId) throws NoSuchCategoryException;

    Collection<Product> getProductsByManufacturer(Long manufacturerId) throws NoSuchManufacturerException;

    Collection<Product> getProductsByCategoryAndManufacturer(Long categoryId, Long manufacturerId) throws NoSuchCategoryException, NoSuchManufacturerException;

    Integer getTotalPriceForCategory(Long categoryId);

    Optional<Accessory> getAccessoryById(Long Id);
}
