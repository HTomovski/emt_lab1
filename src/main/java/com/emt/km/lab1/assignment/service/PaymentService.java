package com.emt.km.lab1.assignment.service;

import com.stripe.exception.*;
import com.stripe.model.Charge;
import com.emt.km.lab1.assignment.model.dto.ChargeRequest;

/**
 * Created by HTomovski
 * Date: 15-May-19
 * Time: 2:57 PM
 */

public interface PaymentService {
    Charge charge(ChargeRequest chargeRequest) throws StripeException;

}
